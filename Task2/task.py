import requests
import beautifulsoup4


url = 'http://books.toscrape.com/index.html'
response = requests.get(url)
content = BeautifulSoup(response.text, "html.parser")
for product in content.findAll('div', attrs={"class": "bookcontainer"}):
    bookObject = {
        "name": book.find('h1', attrs={"class": "col-sm-6 product_main"}).text.encode('utf-8'),
        "price": book.find('p', attrs={"class": "price_color"}).text.encode('utf-8'),
        "image": book.find('img', attrs={"class": "item active"}).text.encode('utf-8'),
        "rating": book.find('p', attrs={"class": "star-rating Three"}).text.encode('utf-8'),
        "in_stock": book.find('p', attrs={"class": "instock availability"}).text.encode('utf-8')
    }
    bookArr.append(bookObject)
with open('bookData.json', 'w') as outfile:
    json.dump(bookArr, outfile)



# def get_products_from_page(url):
#     return []