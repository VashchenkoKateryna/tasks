# def get_temperature_closest_to_zero(temperatures, T):
#     return temperatures[min(range(len(temperatures)), key = lambda i: abs(temperatures[i]-T))]

# temperatures = [1, 2, 100, -0.5, 2, 0.5]
# T = 0
# print(get_temperature_closest_to_zero(temperatures, T))

import pytest

@pytest.fixture(scope="function")
def get_temperature_closest_to_zero(temperatures, T):
    if not temperatures:
        return 0
    r = min(map(abs, temperatures))
    if r in temperatures:
        pass
    else:
        r = -r
    return r

temperatures = [1, 2, 100, -0.5, 2, 0.5]
T = 0
print(get_temperature_closest_to_zero(temperatures, T))

